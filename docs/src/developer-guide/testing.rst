Build and Test commands 
=======================

A build and test commands guide

Build
--------

To build docker image of HDB++ , use following command:
    ``make oci-image-build``


Testing
-------
To test archiver ,use following command:

    ``make k8s-test``


Useful commands
---------------

Command to wait till archiver deployment is complete:
    ``make k8s-wait``

Command to watch all the resources in the namespace:
    ``make k8s-watch``    

Command to delete the namespace:
    ``make k8s-delete-namespace``


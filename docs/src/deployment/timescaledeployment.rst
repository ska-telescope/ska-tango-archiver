TimescaleDB Deployment
======================

TimescaleDB is a pre-requisite for EDA , hence it is needed to be deployed before EDA. If it is deployed already, this step can be skipped.

TimescaleDB for SKA
--------------------

**Introduction**

This directory contains a Helm chart to deploy a single
node TimescaleDB cluster in a
High Availability (HA) configuration on Kubernetes. This chart will do the following:

- Creates two (by default) pods using a Kubernetes statefulset.
- Each pod has a container created using the TimescaleDB docker image.
  - TimescaleDB 2.1 and PG 13
- Each of the containers runs a TimescaleDB instance and Patroni agent.
- Each TimescaleDB instance is configured for replication (1 Master + 1 Replicas).


TimescaleDB is the backend database of the EDA solution. Following table shows the resource configuration of the TimescaleDB server instances for different SKA environments.
Default TimescaleDB values are altered according to the environment where it is getting deployed. 
For eg. in ITF-LOW the values required for Resources and PersistentVolumes are as given below. Using itf-low-values.yaml file
present here ``charts/ska-tango-archiver-timescaledb/environment/itf-low-values.yaml``, TimescaleDB can be deployed. Refer **Installation** section, to install TimescaleDB using yaml file.


+--------------+-----------------------------------------+------------------------------------+
|              |             Resources                   |       PersistentVolumes            |
+==============+======================+==================+========+==========+================+
| Environment  |        Memory(Gi)    |       CPU        | Wal(Gi)| Data(Gi) |   Nodes        |
+--------------+----------+-----------+---------+--------+--------+----------+----------------+
|              | Requests | Limits    |Requests | Limits |        |          |                |
+--------------+----------+-----------+---------+--------+--------+----------+----------------+
| itf-mid      |   16     |   32      |   8     |   16   |   3    |    47    |  za-itf-eda    |
+--------------+----------+-----------+---------+--------+--------+----------+----------------+
| itf-low      |   6      |   8       |   150m  |   200m |   3    |    47    |  au-itf-eda    |
+--------------+----------+-----------+---------+--------+--------+----------+----------------+
| psi-mid      |   4      |   4       |   2     |   2    |   3    |    47    |       NA       |
+--------------+----------+-----------+---------+--------+--------+----------+----------------+
| psi-low      |   4      |   4       |   2     |   2    |   3    |    47    |       NA       |
+--------------+----------+-----------+---------+--------+--------+----------+----------------+
| Dish         |   4      |   4       |   2     |   2    |   3    |    47    |       NA       |
+--------------+----------+-----------+---------+--------+--------+----------+----------------+

**Installation**

  To install the chart with the release name `my-release`, first  in `values.yaml` you need to set credentials mentioned in list
  below. If you decide not to set those credentials, they will be randomly generated. Those credentials can be setup via helm only
  during helm first run and they won't be rotated with subsequent helm update commands to prevent breaking the database.

    The credentials can be found in above sections

    * The credentials for the superuser, admin and stand-by users
    * TLS Certificates
    * pgbackrest config (optional)

    Then you can install the chart with:

    ``helm install <pod-name> charts/timescaledb -n <namespace>``

  You can override parameters using the `--set key=value[,key=value]` argument to `helm install`,
  e.g., to install the chart with backup enabled:

      ``helm install <pod-name> charts/timescaledb -n <namespace> --set backup.enabled=true``

  Alternatively, a YAML file that specifies the values for the parameters can be provided while installing the chart.
    
      ``helm install <pod-name> -f <path to values.yaml> charts/timescaledb``

  In this case, a simple example-values.yaml is present in example folder, this way we can write multiple values.yaml as per specifications

**Installing for different SKA environments**

  This repository provides multiple installation options tailored for needs of different SKA deployment environments. This is done because of differences in the computing infrastructure in each environment. The parameters specific to an environment are maintained in separate yaml files. These files should be given as additional parameters while installing the TimescaleDB in respective environment.
  For example, to install TimescaleDB chart for psi-low specifications, we can use following command
  
    ``helm install timescaledb-1 -f charts/timescaledb/environment/psi-low-values.yaml charts/timescaledb``

  Similarly different values.yaml can be used to deploy TimescaleDB as per the specifications of that environment.

  For details about what parameters you can set, have a look at the [Administrator Guide](docs/admin-guide.md#configure)

**Note**
  * The password field is blank, user need to set it using CI_CD variable or using Vault(Not been tested)\
  * Values required to set for CI/CD variables are:
  
  .. code-block::
    
    --set secrets.credentials.PATRONI_SUPERUSER_PASSWORD="$(CI_CD_VARIABLE_NAME)" 
    --set secrets.credentials.PATRONI_REPLICATION_PASSWORD="$(CI_CD_VARIABLE_NAME)" 
    --set secrets.credentials.PATRONI_admin_PASSWORD="$(CI_CD_VARIABLE_NAME)" 
  
  * To use vault, please update values.yaml accordingly:
  
  .. code-block::

    timescaledb:
      vault:
        useVault: true
        secretPath: <secret_path>
        role: kube-role
  
  * or you can use the set option for vault:
  
  .. code-block::

    --set timescaledb.vault.useVault=$(VAULT_ENABLED)
    --set timescaledb.vault.secretPath=$(SECRET_PATH)
    --set timescaledb.vault.role=$(VAULT_ROLE)
    


**Cleanup**

  To remove the spawned pods you can run a simple
  
    ``helm delete my-release``

  Some items, (pvc's and S3 backups for example) are not immediately removed.
  To also purge these items, have a look at the [Administrator Guide](docs/admin-guide#cleanup)

**Further reading**
 
  :doc:`admin-guide`
Deployment
==========

The EDA can be deployed in following ways as shown below. 

**Note:**
The TimescaleDB is not the deployment which is done frequently. TimescaleDB should be deployed when its not functional or there are updates in chart.

.. toctree::
   :maxdepth: 3

   timescaledeployment
   edadeployment
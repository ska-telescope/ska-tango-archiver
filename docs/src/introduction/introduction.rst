Getting started
=================


| This is the documentation for SKA Engineering Data Archiver[EDA].

| The SKA EDA solution is based on HDB++ (Historical Data Base++), which is a standard archiver tool in Tango ecosystem used for **archiving tango attributes** . Further reading will guide through the usage of attributes archiving techniques, as well as extraction techniques to extract archived attributes.

| HDB++ inherits the database structure from the existing Tango Historical Data Base (HDB) and introduces new storage architecture possibilities, better internal diagnostic capabilities and an optimized API. 
  Among the various backend databases that HDB++ supports the SKA EDA solution uses TimescaleDB as the backend database.

| The HDB++ archiving system must fully comply to the Tango device server model, with two immediate benefits.
| First, all the required configuration parameters are stored to and retrieved from the Tango database; some of these parameters are, for user convenience, duplicated into a dedicated table of the HDB++ schema by a mechanism that guarantees the consistency of the copy.
| Second,the HDB++ archiving system inherits the Tango scaling capability: any number of EventSubscriber instances can be deployed according to the desired architecture and overall performance.


Main Components SKA Engineering Data Archiver are:

1. `Configuration Manager <https://tango-controls.readthedocs.io/en/latest/administration/services/hdbpp/hdbpp-cm-interface.html>`_
2. `Event Subscriber <https://tango-controls.readthedocs.io/en/latest/administration/services/hdbpp/hdbpp-es-interface.html>`_


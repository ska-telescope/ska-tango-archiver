# SKA Archiver

This project provides archiving solution for SKA. The archiver solution is based on HDB++ Archiver provided by Tango community. The repository contains set of Dockerfiles, helm charts and scripts that are useful for building, deploying and configuring the HDB++ archiver.

## Build

To build docker image of HDB++ navigate to `docker` directory from terminal and enter command:

```commandline
    make oci-image-build
```

## Testing

```commandline
    make k8s-test
```

## Deployment

#### Values required

| Key         | type        | default              |
| ----- | ----------- | ------------------   |
| dbname      | str         |  default_archiver_db |
| hostname   | str        | None|
|dbpassword| str | None |
|dbuser| str | None |
|port| int | None |
|telescope| str | SKA-mid |
|telescope_environment| str | MID-STFC |
|archwizard_config| str | "MyHDB=tango://tango-databaseds.ska-tango-archiver.svc.cluster.local:10000/mid-eda/cm/01"|


### To deploy the archiver enter following command:

```Update
    make k8s-install-chart ARCHIVER_DBNAME=<dbname> ARCHIVER_TIMESCALE_HOST_NAME=<hostname> ARCHIVER_TIMESCALE_PORT=<port> ARCHIVER_TIMESCALE_DB_USER=<dbuser> ARCHIVER_TIMESCALE_DB_PWD=<dbpassword>

```

The archiver gets deployed in namespace `ska-tango-archiver`

On gitlab ci-cd Masked Environment variables are created `ARCHIVER_TIMESCALE_DB_PWD` `ARCHIVER_TIMESCALE_DB_USER` `ARCHIVER_TIMESCALE_HOST_NAME` `ARCHIVER_TIMESCALE_PORT` which contains archiver database connection details.

### To deploy the archiver using local archiver DB details, enter following command:

```commandline
    make k8s-install-chart ARCHIVER_DBNAME=<dbname> ARCHIVER_TIMESCALE_HOST_NAME=<hostname> ARCHIVER_TIMESCALE_PORT=<port> ARCHIVER_TIMESCALE_DB_USER=<dbuser> ARCHIVER_TIMESCALE_DB_PWD=<dbpassword>
```


### To delete the deployment enter following command:

```commandline
    make k8s-uninstall-chart
```
* Local Deployment and testing:

    1.Connect to STFC VPN.

    2.Update database details present in data/configuration.json file.
      currently host name : timescaledb.ska-eda-mid-db.svc.cluster.local / timescaledb.ska-eda-low-db.svc.cluster.local
      NOTE:if not able to connect please check k8s-test in pipeline for ip address, might be due to redeployment of timescaledb.

    3.Deploy using folowing command, pass db details if different then the default one.
     ```
     make k8s-install-chart ARCHIVER_DBNAME=<dbname> ARCHIVER_TIMESCALE_HOST_NAME=10.200.10.143/timescaledb.ska-eda-mid-db.svc.cluster.local ARCHIVER_TIMESCALE_PORT=<port> ARCHIVER_TIMESCALE_DB_USER=<dbuser> ARCHIVER_TIMESCALE_DB_PWD=<dbpassword>
     ```
     for low telescope:
     ```
     make k8s-install-chart TELESCOPE=ska-low ARCHIVER_DBNAME=<dbname> ARCHIVER_TIMESCALE_HOST_NAME_LOW=10.200.12.14/timescaledb.ska-eda-low-db.svc.cluster.local ARCHIVER_TIMESCALE_PORT_LOW=<port> ARCHIVER_TIMESCALE_DB_USER_LOW=<dbuser> ARCHIVER_TIMESCALE_DB_PWD_LOW=<dbpassword>

     ```

    4.To test the deployment:

    ```
    make k8s-test

    ```


### Useful commands

Command to wait till archiver deployment is complete:

```commandline
    make k8s-wait
```

Command to delete the namespace:

```commandline
    make k8s-delete-namespace
```

## Access the TimescaleDB instance in STFC

* To access the timescaleDB instance in STFC, please check latest k8s-test job(line 57) to get the latest external IP and port.

* The external IP for mid : 10.200.10.143 or timescaledb.ska-eda-mid-db.svc.cluster.local
    external IP for low: 10.200.12.14 or timescaledb.ska-eda-low-db.svc.cluster.local
    port : 5432 are for current deployment.
    If it is redeployed, make sure to update it with latest external IP.

* **Install postgres client on user machine** :Follow the steps in the given [link](https://www.postgresql.org/download/linux/ubuntu/) for installation.

* Make sure the user's machine is connected to SFTC vpn.

* Enter below command to access the TimescaleDB.

```commandline
    psql -U admin -h <external-ip> -p <port> -d <db-name>
```
* Where default db-name is postgres and branch specific db-name can be ska_archiver_<branch_name> ,for eg : ska_archiver_hm_72.

* Enter the password to access the DB.

## Configuring new attributes using Yaml2archiving

### Steps to deploy locally and access configurator tool.

1. Once deployment is completed, execute ``minikube service configurator -n <KUBE_NAMESPACE> --url`` to get the url of configuration tool.
2. Click on the link, then the title page that would be followed by configurator tool UI as shown below will appear.

    ![EDA](/docs/src/eda_config_1.png "EDA Confgurator UI")

### Steps to access configurator deployed inside cluster.

1. Once deployment is completed, the link from the test job can be used to to access configurator tool. The link would be

```
    http://{INGRESS_HOST}/{KUBE_NAMESPACE}/configurator/
```

    INGRESS_HOST: Host for accessing the tool; k8s.stfc.skao.int;
    KUBE_NAMESPACE: Namespace of the deployment

2. Link will load the UI as shown above.

### UI DETAILS:
The UI consists of:
1. Radio options :
    i. Add/update - This option adds attributes present in the yaml file or changes the configuration if the attributes are present already. \
    ii. Remove - This option removes attributes present in the yaml file.
2. File Upload box : This option helps to upload the user's yaml file. Only yaml files are allowed to be uploaded.
3. input text: This text accepts event subscriber TRL(Tango Resource Locator) which will be used to provide current configuration.
4. Download button: click once to download configuration file of given event subscriber.
5. Reset Table button: Click once to erase the contents of the table.
6. Refresh Table button : Once clicked will sync table with latest operations performed with other clients.
7. Table : This will be visible once you perform any operation like add/remove/update.The table consists of timestamp, operation performed and Attribute name.

### HOW TO CONFIGURE:

1. User can upload the yaml file containing attribute configurations and choose one of the option [add/update,remove].
2. Once submitted , user will able to see table as shown below containing data of attributes archived in the system.

    ![EDA](docs/src/eda_config3.png "EDA Confgurator UI after configuration")

### HOW TO DOWNLOAD:

User can enter event subscriber TRL(Tango Resource Locator) in the input box and then click download button to get configuration present in the system.

### API DETAILS:

1. configure-archiver:
      This API helps to configure archiver using yaml file and options["add_update"/"remove"].
    ```
         curl -X 'POST' \
        'http://<IP>:8003/<KUBE NAMESPACE>/configure-archiver' \
        -H 'accept: application/json' \
        -H 'Content-Type: multipart/form-data' \
        -F 'file=@<YAML FILE>;type=application/x-yaml' \
        -F 'option=add_update'
    ```

    ```
        curl -X 'POST' \
        'http://<IP>:8003/<KUBE NAMESPACE>/configure-archiver' \
        -H 'accept: application/json' \
        -H 'Content-Type: multipart/form-data' \
        -F 'file=@<YAML FILE>;type=application/x-yaml' \
        -F 'option=remove'
    ```
2. download-configuration:
      This API accepts eventsubscriber name and provides yaml file with current attributes archived into the system.

    ```
        curl -X 'GET' \
        'http://<IP>:8003/<KUBE NAMESPACE>/download-configuration/?eventsubscriber=mid-eda%2Fes%2F01' \
        -H 'accept: application/json'
    ```
3. [GET]configurations:
      This API provides history of all the operations performed in the system.

    ```
        curl -X 'GET' \
        'http://<IP>:8003/<KUBE NAMESPACE>/configurations' \
        -H 'accept: application/json'
    ```
4. [DELETE]configurations:
      This API deletes history of operations performed.

    ```
        curl -X 'DELETE' \
        'http://<IP>:8003/<KUBE NAMESPACE>/configurations' \
        -H 'accept: application/json'
    ```

## ArchWizard
* To access the archwizard instance in STFC, get the latest external IP and port from k8s-test job. Make sure STFC VPN connection is on
* OR, After getting KUBECONFIG file locally run command as shown below to get details of service.
```commandline
    kubectl --KUBECONFIG=<kubeconfig_filename> get svc
```
* Copy the external-ip and port as shown below and paste it in new tab in browser.
```commandline
    <external-ip>:<port>
```

## To retrieve archived data using web based GUI Archviewer

* Steps to retrieve data.
    * To access the archviewer instance in STFC, get the latest external IP and port from k8s-test job. Make sure STFC VPN connection is on
    * OR, After getting KUBECONFIG file locally run command as shown below to get details of service.
    ```commandline
        kubectl --KUBECONFIG=<kubeconfig_filename> get svc
    ```
    * Copy the external-ip and port as shown below and paste it in new tab in browser.
    ```commandline
       <external-ip>:<port>
    ```
## Data Extraction

* libhdbpp-python CLI:

Pre-requisite:
    1. PyYAML
    2. psycopg2

1. INSTALLATION Steps:
    1. clone https://gitlab.com/tango-controls/hdbpp/libhdbpp-python
    2. change directory to libhdbpp-python  and run following command
    ```
    pip install .

    ```
* OR
    1. Run make target from ska-tango-archiver repo ``` make install-libhdbpp-python ```

2. Usage Manual

    ```
    hdbpp-reader [-h] [-v] [-D] [--syslog] [-b BACKEND] [-P] [-c CONFIG] [-C CONNECT] [-d DATABASE] [-H HOST] [-u USER] [-p PASSWORD]
                        [-t TIMEFORMAT]
                        {list,read} ...

    for timescaledb use --backend argument as follows:
    --backend=pyhdbpp.timescaledb.timescaledb.TimescaleDbReader

    ```

    1. connection details can be provided using connection string ,config file or argument:

    ```
    host: <timescaledb_host_name>
    database: <database name>
    user: <database user>
    password :<database password>
    port: <port>

    ```
    2. List Function: to list attributes:
    -P --pattern = this option can be used to search pattern

    ```
    using config file for connection:
    hdbpp-reader --backend=pyhdbpp.timescaledb.timescaledb.TimescaleDbReader -c config.yaml list -P "*"

    using connection details as arguments:

    hdbpp-reader --backend=pyhdbpp.timescaledb.timescaledb.TimescaleDbReader  list -P "*"
    host (default localhost):10.200.10.143 or timescaledb.ska-eda-mid-db.svc.cluster.local <for mid>
                            or
    host(default localhost):10.200.12.14 or timescaledb.ska-eda-low-db.svc.cluster.local <for low>

    port (default 3306):5432
    database (default hdb):ska_archiver_hm_71
    user:admin
    password:admin
    output:
    2022-11-07 17:17:21 hdbpp_reader[685660]: Loaded config file: config.yaml
    tango://tango-host-databaseds-from-makefile-test.ci-ska-tango-archiver-965447b4.svc.cluster.local:10000/sys/tg_test/1/double_scalar


    Listing attributes double_scalar:
    hdbpp-reader --backend=pyhdbpp.timescaledb.timescaledb.TimescaleDbReader -c config.yaml list -P "*/double_scalar"

    Listing attributes of test device:
    hdbpp-reader --backend=pyhdbpp.timescaledb.timescaledb.TimescaleDbReader -c config.yaml list -P "*/sys/tg_test/1/*"


    ```
    3. READ Function: to list attributes:
    -P --pattern = this option can be used to search pattern

    ```
    using config file for connection:
    hdbpp-reader --backend=pyhdbpp.timescaledb.timescaledb.TimescaleDbReader -c config.yaml read <attribute_full_name>

    for example:
    hdbpp-reader --backend=pyhdbpp.timescaledb.timescaledb.TimescaleDbReader -c config.yaml read tango://tango-host-databaseds-from-makefile-test.ci-ska-tango-archiver-e6c90079.svc.cluster.local:10000/sys/tg_test/1/double_scalar
    output:
    2022-11-07 17:36:00 hdbpp_reader[705016]: Loaded config file: config.yaml
    (datetime.datetime(2022, 11, 7, 11, 29, 1, 489121, tzinfo=datetime.timezone.utc), 87.55708576347412, 0.0, 0, None)


    using connection details as arguments:

    hdbpp-reader --backend=pyhdbpp.timescaledb.timescaledb.TimescaleDbReader  read <attribute_full_name>
    host (default localhost):10.200.10.143 or timescaledb.ska-eda-mid-db.svc.cluster.local <for mid>
                                    or
    host (default localhost):10.200.12.14 or timescaledb.ska-eda-low-db.svc.cluster.local <for low>
    port (default 3306):5432
    database (default hdb):ska_archiver_hm_71
    user:admin
    password:admin



3. Using Python Console:
```
    For using Python Console please refer Timescale DB Reader Jupyter Notebook (timescaledb_reader.ipynb)
```

# TimescaleDB for SKA
## Introduction
This directory contains a Helm chart to deploy a two
node TimescaleDB cluster in a
High Availability (HA) configuration on Kubernetes. This chart will do the following:

- Creates two (by default) pods using a Kubernetes statefulset.
- Each pod has a container created using the TimescaleDB docker image.
  - TimescaleDB 2.1 and PG 13
- Each of the containers runs a TimescaleDB instance and Patroni agent.
- Each TimescaleDB instance is configured for replication (1 Master + 1 Replicas).


## Installation

To install the chart with the release name `my-release`, first  in `values.yaml` you need to set credentials mentioned in list
below. If you decide not to set those credentials, they will be randomly generated. Those credentials can be setup via helm only
during helm first run and they won't be rotated with subsequent helm update commands to prevent breaking the database.

The credentials can be found in above sections

* The credentials for the superuser, admin and stand-by users
* TLS Certificates
* pgbackrest config (optional)

Then you can install the chart with:
```console
helm install <pod-name> charts/ska-tango-archiver-timescaledb -n <namespace>
```

You can override parameters using the `--set key=value[,key=value]` argument to `helm install`,
e.g., to install the chart with backup enabled:

```console
helm install <pod-name> charts/ska-tango-archiver-timescaledb -n <namespace> --set backup.enabled=true
```

Alternatively, a YAML file that specifies the values for the parameters can be provided while installing the chart.
```console
helm install <pod-name> -f <path to values.yaml> charts/ska-tango-archiver-timescaledb
```
In this case, a simple example-values.yaml is present in example folder, this way we can write multiple values.yaml as per specifications

### Installing for different SKA environments
This repository provides multiple installation options tailored for needs of different SKA deployment environments. This is done because of differences in the computing infrastructure in each environment. The parameters specific to an environment are maintained in separate yaml files. These files should be given as additional parameters while installing the TimescaleDB in respective environment.
For example, to install timescaledb chart for psi-low specifications, we can use following command
```console
helm install timescaledb-1 -f charts/ska-tango-archiver-timescaledb/environment/psi-low-values.yaml charts/ska-tango-archiver-timescaledb
```
Similarly different values.yaml can be used to deploy timescaledb as per the specifications of that environment.

For details about what parameters you can set, have a look at the [Administrator Guide](docs/admin-guide.md#configure)

## Cleanup

To remove the spawned pods you can run a simple
```console
helm delete my-release
```
Some items, (pvc's and S3 backups for example) are not immediately removed.
To also purge these items, have a look at the [Administrator Guide](docs/admin-guide.md#cleanup)

## Further reading

- [Administrator Guide](docs/admin-guide.md)

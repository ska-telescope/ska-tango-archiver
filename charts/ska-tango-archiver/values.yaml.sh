#!/usr/bin/env bash

RELEASE_SUPPORT="${RELEASE_SUPPORT:-../../.make/.make-release-support}"
VALUES_YAML=values.yaml
CAR_OCI_REGISTRY_HOST="${CAR_OCI_REGISTRY_HOST:-artefact.skao.int}"
CI_COMMIT_SHORT_SHA="${CI_COMMIT_SHORT_SHA:-blah}"
SUFFIX=""

# Disabled this as it complicates the rules: changes: - pxh 30/09/2021
# Check if this is a dev build
if [[ "${CAR_OCI_REGISTRY_HOST}" == registry.gitlab.com* ]] || [[ -z "${CAR_OCI_REGISTRY_HOST}" ]]; then
    SUFFIX="-dev.c${CI_COMMIT_SHORT_SHA}"  #"-" is used as "+" causes the docker building process to fail
fi

cat <<EOF > ${VALUES_YAML}
# Default values for HDB++ Archiver.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

display: ":0"
xauthority: "~/.Xauthority"
enabled: true

legacy_compatibility: true

system: SW-infrastructure
subsystem: ska-tango-archiver
telescope: SKA-mid
telescope_environment: MID-STFC
car_sources: ""
car_file_path: ""
configuration_file_url: ""
allowed_origins:
  - http://k8s.stfc.skao.int
  - https://k8s.stfc.skao.int

global:
  minikube: false
  operator: true
  exposeAllDS: false
  tango_host: databaseds-tango-base-{{.Release.Name}}:10000


dbname: default_archiver_db
archwizard_config: "MyHDB=tango://tango-databaseds.ska-tango-archiver.svc.cluster.local:10000/mid-eda/cm/01"

labels:
  app: ska-tango-archiver
  
annotations:
  app.gitlab.com/app: CI_PROJECT_PATH_SLUG
  app.gitlab.com/env: CI_ENVIRONMENT_SLUG

dsconfig:
  image:
    registry: artefact.skao.int
    image: ska-tango-images-tango-dsconfig
    tag: 1.5.7
    pullPolicy: IfNotPresent

itango:
  image:
    registry: artefact.skao.int
    image: ska-tango-images-tango-itango
    tag: 9.3.10
    pullPolicy: IfNotPresent

postgres:
  image:
    registry: hub.docker.com
    image: postgres
    tag: 14.5
    pullPolicy: Always

archiver_image:
  image:
    registry: ${CAR_OCI_REGISTRY_HOST}
    image: ska-tango-archiver
    tag: $(. ${RELEASE_SUPPORT}; RELEASE_CONTEXT_DIR=../../images/ska-tango-archiver setContextHelper; getVersion)${SUFFIX}
    pullPolicy: IfNotPresent

archviewer:
  instances:
  - name: "mid"
    timescale_host: "customhost"
    timescale_databases: ""
    timescale_login: "dummyuser:dummypass"

  - name: "low"
    timescale_host: "customhost"
    timescale_databases: ""
    timescale_login: "dummyuser:dummypass"

hdbppdb:
  enabled: true
  use_pv: false
  storage_size: 100Gi
  cleaning_schedule: "* 6,18 * * *"
  component: db
  function: ska-tango-archiver
  domain: ska-tango-archiver
  intent: production
  livenessProbe:
    enabled: false
    initialDelaySeconds: 0
    periodSeconds: 10
    timeoutSeconds: 1
    successThreshold: 1
    failureThreshold: 3
  readinessProbe:
    enabled: false
    initialDelaySeconds: 0
    periodSeconds: 10
    timeoutSeconds: 1
    successThreshold: 1
    failureThreshold: 3
        
deviceServers:
  archiver-cm:
    enabled: true
    mid:
      name: "mid-eda/cm/01"
      file: "data/archiver-cm-mid.yaml"
    low:
      name: "low-eda/cm/01"
      file: "data/archiver-cm-low.yaml"

  archiver-es:
    enabled: true
    mid:
      name: "mid-eda/es/01"
      file: "data/archiver-es-mid.yaml"
    low:
      name: "low-eda/es/01"
      file: "data/archiver-es-low.yaml"


      
nodeSelector: {}

affinity: {}

tolerations: []

EOF

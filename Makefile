-include .make/base.mk
-include .make/k8s.mk
-include .make/python.mk
-include .make/helm.mk
-include .make/oci.mk
-include PrivateRules.mak

BASE = $(shell pwd)
TELESCOPE ?= SKA-mid
KUBE_NAMESPACE ?= ska-tango-archiver#namespace to be used
RELEASE_NAME ?= test## release name of the chart
K8S_CHART ?= ska-tango-archiver# Path of the umbrella chart to work with
CONFIGURE_ARCHIVER = test-configure-archiver
PYTHON_LINE_LENGTH = 99
PYTHON_LINT_TARGET ?= tests/ src/ charts/ska-tango-archiver/data/auto_configure.py
MINIKUBE ?= false## Minikube or not
K8S_TEST_IMAGE_TO_TEST = artefact.skao.int/ska-tango-images-tango-itango:9.3.10 ## docker image that will be run for testing purpose
CI_JOB_ID ?= local##pipeline job id
TEST_RUNNER ?= test-mk-runner-$(CI_JOB_ID)##name of the pod running the k8s_tests
TANGO_HOST ?=  tango-host-databaseds-$(RELEASE_NAME):10000## TANGO_HOST is an input!
TANGO_SERVER_PORT ?= 45450## TANGO_SERVER_PORT - fixed listening port for local server
K8S_CHARTS ?= ska-tango-archiver ska-tango-archiver-timescaledb## list of charts to be published on gitlab -- umbrella charts for testing purpose
ARCHIVER_DBNAME ?= default_archiver_timescaledb
CI_PROJECT_PATH_SLUG ?= ska-tango-archiver
CI_ENVIRONMENT_SLUG ?= ska-tango-archiver
ATTR_CONF?= attribute_config.yaml
CLUSTER_DOMAIN ?= cluster.local
ARCHIVER_TIMESCALE_HOST_NAME_MID?=timescaledb.ska-eda-mid-db.svc.$(CLUSTER_DOMAIN)#for testing
ARCHIVER_TIMESCALE_PORT_MID?=5432#for testing
ARCHIVER_TIMESCALE_DB_USER_MID?=admin#for testing
ARCHIVER_TIMESCALE_DB_PWD_MID?=admin#timescaledb admin password
ARCHWIZARD_VIEW_DBNAME?= MyHDB
TANGO_HOST_NAME?= $(shell echo $(TANGO_HOST) | cut -d ":" -f 1)
StorageClass?= "bds1"
REPLICA_COUNT_MID?= 2
REPLICA_COUNT_LOW ?= 2
TIMESCALE_IMAGE ?= pg14.6-ts2.8.1-patroni-static-primary-p0
TIMESCALE_CHART_VERSION ?= 0.27.3
EVENT_SUBSCRIBER ?=#event receiver fqdn
CONFIG_MANAGER?=#configuration manager fqdn without quotes because used for ARCHWIZARD_CONFIG
ARCHIVER_TIMESCALE_HOST_NAME_LOW?=timescaledb.ska-eda-low-db.svc.$(CLUSTER_DOMAIN)#for testing
ARCHIVER_TIMESCALE_PORT_LOW?=5432#for testing
ARCHIVER_TIMESCALE_DB_USER_LOW?=admin#for testing
ARCHIVER_TIMESCALE_DB_PWD_LOW?=#timescaledb admin password for low deployment
ARCHWIZARD_CONFIG?= $(ARCHWIZARD_VIEW_DBNAME)=tango://$(TANGO_HOST_NAME).$(KUBE_NAMESPACE).svc.$(CLUSTER_DOMAIN):10000/$(CONFIG_MANAGER)
TELESCOPE_ENVIRONMENT ?=
INGRESS_HOST = k8s.stfc.skao.int#Default ingress host
ARCHIVER_TIMESCALE_DB_SUPERUSER_PWD_MID?=#super user password of timescaledb
ARCHIVER_TIMESCALE_DB_SUPERUSER_PWD_LOW?=#super user password of timescaledb for low deployment
OCI_IMAGE_BUILD_CONTEXT=$(PWD)
VAULT_ENABLED ?= false 
SECRET_PATH?=# enter vault secret path
VAULT_ROLE?=kube-role
CAR_FILE_PATH ?=# car file path required by auto configure job
K8S_TIMEOUT=360s
ifeq ("$(TELESCOPE)", "SKA-low")
EVENT_SUBSCRIBER = "low-eda/es/01"
CONFIG_MANAGER = low-eda/cm/01
ARCHIVER_TIMESCALE_HOST_NAME = "$(ARCHIVER_TIMESCALE_HOST_NAME_LOW)" #for testing
ARCHIVER_TIMESCALE_PORT = $(ARCHIVER_TIMESCALE_PORT_LOW)#for testing
ARCHIVER_TIMESCALE_DB_USER =$(ARCHIVER_TIMESCALE_DB_USER_LOW)#for testing
ARCHIVER_TIMESCALE_DB_PWD = $(ARCHIVER_TIMESCALE_DB_PWD_LOW)
ARCHIVER_TIMESCALE_DB_SUPERUSER_PWD = $(ARCHIVER_TIMESCALE_DB_SUPERUSER_PWD_LOW)
REPLICA_COUNT = $(REPLICA_COUNT_LOW)
CAR_FILE_PATH = "archiver_configuration/low_configuration/archiver_configuration.yaml"
endif
ifeq ("$(TELESCOPE)", "SKA-mid")
EVENT_SUBSCRIBER = "mid-eda/es/01"
CONFIG_MANAGER = mid-eda/cm/01
ARCHIVER_TIMESCALE_HOST_NAME = "$(ARCHIVER_TIMESCALE_HOST_NAME_MID)" #for testing
ARCHIVER_TIMESCALE_PORT = $(ARCHIVER_TIMESCALE_PORT_MID)#for testing
ARCHIVER_TIMESCALE_DB_USER =$(ARCHIVER_TIMESCALE_DB_USER_MID)#for testing
ARCHIVER_TIMESCALE_DB_PWD = $(ARCHIVER_TIMESCALE_DB_PWD_MID)
ARCHIVER_TIMESCALE_DB_SUPERUSER_PWD = $(ARCHIVER_TIMESCALE_DB_SUPERUSER_PWD_MID) 
REPLICA_COUNT = $(REPLICA_COUNT_MID)
CAR_FILE_PATH = "archiver_configuration/mid_configuration/archiver_configuration.yaml"
RELEASE_CONTEXT_DIR=$(PWD)/images/ska-tango-archiver
endif
MARK ?= SKA_mid
ADD_ARGS ?=
ifeq ($(MAKECMDGOALS),k8s-test)
MARK = $(shell echo $(TELESCOPE) | sed s/-/_/) and (post_deployment or acceptance)
endif
FILE ?= tests
PYTHON_TEST_FILE ?=

# Set the specific environment variables required for pytest
PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=${PYTHONPATH}:/app:/app/tests KUBE_NAMESPACE=$(KUBE_NAMESPACE) HELM_RELEASE=$(RELEASE_NAME) TANGO_HOST=$(TANGO_HOST) CLUSTER_DOMAIN=$(CLUSTER_DOMAIN)
PYTHON_VARS_AFTER_PYTEST = -m '$(MARK)' $(ADD_ARGS) $(FILE)

K8S_TEST_TEST_COMMAND = $(PYTHON_VARS_BEFORE_PYTEST) $(PYTHON_RUNNER) \
						pytest \
						$(PYTHON_VARS_AFTER_PYTEST) ./tests \
						 | tee pytest.stdout

ifeq ($(K8S_CHART),ska-tango-archiver)

	K8S_CHART_PARAMS = --set global.minikube=$(MINIKUBE) \
	--set global.exposeAllDS=$(MINIKUBE) \
	--set global.tango_host=$(TANGO_HOST) \
	--set telescope=$(TELESCOPE) \
	--set hostname=$(ARCHIVER_TIMESCALE_HOST_NAME) \
	--set dbname=$(ARCHIVER_DBNAME) \
	--set port=$(ARCHIVER_TIMESCALE_PORT) \
	--set dbuser=$(ARCHIVER_TIMESCALE_DB_USER) \
	--set dbpassword=$(ARCHIVER_TIMESCALE_DB_PWD) \
	--set archwizard_config=$(ARCHWIZARD_CONFIG) \
	--set configuration_manager="$(CONFIG_MANAGER)"\
	--set event_subscriber=$(EVENT_SUBSCRIBER)\
	--set telescope_environment=$(TELESCOPE_ENVIRONMENT)\
	--set configuration_file_url=""\
	--set car_sources="car:ska-tmc/ska-tmc-simulators?main"\
	--set car_file_path=$(CAR_FILE_PATH)\
	--set global.cluster_domain=$(CLUSTER_DOMAIN)\
	--set global.operator=true
endif

ifeq ($(K8S_CHART),ska-tango-archiver-timescaledb)

	K8S_CHART_PARAMS = --set secrets.credentials.PATRONI_SUPERUSER_PASSWORD="$(ARCHIVER_TIMESCALE_DB_SUPERUSER_PWD)" \
    --set secrets.credentials.PATRONI_REPLICATION_PASSWORD="$(ARCHIVER_TIMESCALE_DB_PWD)" \
	--set secrets.credentials.PATRONI_admin_PASSWORD="$(ARCHIVER_TIMESCALE_DB_PWD)" \
	--set replicaCount=$(REPLICA_COUNT)\
	--set timescaledb.vault.useVault=$(VAULT_ENABLED)\
	--set timescaledb.vault.secretPath=$(SECRET_PATH)\
	--set timescaledb.vault.role=$(VAULT_ROLE)
endif
	
# PYTHON_VARS_AFTER_PYTEST = --disable-pytest-warnings --timeout=300

RELEASE_SUPPORT := $(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))/.make-release-support


# include OCI Images support

clean: ## clean out references to chart tgz's
	@rm -f ./*/charts/*.tgz ./*/Chart.lock ./*/requirements.lock

check-dbname: ## Check if database name is set to default: default_archiver_db
	@if [ "$(ARCHIVER_DBNAME)" = "default_archiver_db" ]; then \
	echo "Archiver database name is not provided. Setting archiver database name to default value: default_archiver_db"; \
	fi
	@if [ "$(ARCHIVER_HOST_NAME)" = "" ]; then \
	echo "Archiver host name is not provided."; \
	fi
	@if [ "$(ARCHIVER_PORT)" = "" ]; then \
	echo "Archiver port name is not provided."; \
	fi
	@if [ "$(ARCHIVER_DB_USER)" = "" ]; then \
	echo "Archiver database user is not provided."; \
	fi
	@if [ "$(ARCHIVER_DB_PWD)" = "" ]; then \
	echo "Archiver database password is not provided."; \
	fi
k8s-pre-install-chart: check-dbname

eda-configurator-link:
	@echo "############################################################################"
	@echo "#            Access the EDA Configurator page here:"
	@echo "#            https://$(INGRESS_HOST)/$(KUBE_NAMESPACE)/configurator/"
	@echo "############################################################################"

eda-archviewer-link:
	@echo "############################################################################"
	@echo "#            Access the EDA Archviewer page here:"
	@echo "#            https://$(INGRESS_HOST)/$(KUBE_NAMESPACE)/archviewer/"
	@echo "############################################################################"

eda-archwizard-link:
	@echo "############################################################################"
	@echo "#            Access the EDA Archwizard page here:"
	@echo "#            https://$(INGRESS_HOST)/$(KUBE_NAMESPACE)/archwizard/"
	@echo "############################################################################"


get-service:
	$(eval DBARCHIVERSERVICE := $(shell kubectl get svc -n $(KUBE_NAMESPACE) | grep 10000 |  cut -d " " -f 1)) \
	echo $(DBARCHIVERSERVICE);

package: helm-pre-publish ## package charts
	@echo "Packaging helm charts. Any existing file won't be overwritten."; \
	mkdir -p ./tmp
	@for i in $(CHARTS); do \
		helm package charts/$${i} --dependency-update --destination ../tmp > /dev/null; \
	done; \
	mkdir -p ./repository && cp -n ../tmp/* ../repository; \
	cd ./repository && helm repo index .; \
	rm -rf ./tmp

helm-pre-publish: ## hook before helm chart publish
	@echo "helm-pre-publish: generating charts/ska-tango-archiver/values.yaml"
	@cd charts/ska-tango-archiver && bash ./values.yaml.sh

helm-pre-build: helm-pre-publish

helm-pre-lint: helm-pre-publish ## make sure auto-generate values.yaml happens

# use pre update hook to update chart values
k8s-pre-install-chart:
	make helm-pre-publish

k8s-pre-template-chart:
	make helm-pre-publish

show:
	echo $$TANGO_HOST

cred:
	make k8s-namespace
	curl -s https://gitlab.com/ska-telescope/templates-repository/-/raw/master/scripts/namespace_auth.sh | bash -s $(SERVICE_ACCOUNT) $(KUBE_NAMESPACE) || true

install_libhdbpp-python:
	git clone https://gitlab.com/tango-controls/hdbpp/libhdbpp-python.git
	cd libhdbpp-python
	pip install .

k8s-pre-test: python-pre-test test-requirements
	bash ./tests/resources/config_files/attribute_config.sh $(TANGO_HOST_NAME) $(KUBE_NAMESPACE) $(CLUSTER_DOMAIN)
	bash ./tests/resources/config_files/removed_attribute.sh $(TANGO_HOST_NAME) $(KUBE_NAMESPACE) $(CLUSTER_DOMAIN)
	bash ./tests/resources/config_files/attribute_config_low.sh $(TANGO_HOST_NAME) $(KUBE_NAMESPACE) $(CLUSTER_DOMAIN)
	bash ./tests/resources/config_files/removed_attribute_low.sh $(TANGO_HOST_NAME) $(KUBE_NAMESPACE) $(CLUSTER_DOMAIN)
	bash ./tests/resources/config_files/change_attribute.sh $(TANGO_HOST_NAME) $(KUBE_NAMESPACE) $(CLUSTER_DOMAIN)
	bash ./tests/resources/config_files/change_attribute_low.sh $(TANGO_HOST_NAME) $(KUBE_NAMESPACE) $(CLUSTER_DOMAIN)

test-requirements:
	@poetry export --without-hashes --dev --format requirements.txt --output tests/requirements.txt


requirements: ## Install Dependencies
	poetry install

.PHONY: help


# .PHONY is additive

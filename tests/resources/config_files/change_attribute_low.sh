#!/usr/bin/env bash
cat << EOF > tests/resources/config_files/change_attribute_low.yaml
db: $1.$2.svc.$3:10000
manager: low-eda/cm/01
archiver: low-eda/es/01
configuration:
  - class: TangoTest
    attributes:
      float_image:
        archive_period: 8000
        polling_period: 3000
        archive_rel_change: 5

EOF
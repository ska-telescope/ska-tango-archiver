"""
This Module is used for testing EDA archviewer API.
"""

import os

import httpx
import pytest

namespace = os.getenv("KUBE_NAMESPACE")
cluster_domain = os.getenv("CLUSTER_DOMAIN")


@pytest.mark.post_deployment
@pytest.mark.SKA_low
@pytest.mark.SKA_mid
def test_archviewer_api():
    """test to check archviewer api"""
    response = httpx.get(f"http://archviewer.{namespace}.svc.{cluster_domain}:8082")
    assert response.status_code == 200

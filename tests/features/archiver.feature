#check device is ON
Scenario: Test Configuration Manager device is ON
	Given a device called mid-eda/cm/01
	When I call the command state()
	Then the attribute DevState is ON

Scenario: Test Event Subscriber device is ON
	Given a device called mid-eda/es/01
	When I call the command state()
	Then the attribute DevState is ON

#check archiving is started
Scenario: Check archiving
	Given a Configuration Manager called mid-eda/cm/01 and an Event Subscriber called mid-eda/es/01
	When I request to archive the attribute sys/tg_test/1/double_scalar
	Then after 1000 milliseconds the Archiving is Started

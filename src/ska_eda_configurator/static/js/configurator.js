var base_url = window.location.href.replace("configuration-page", "");
window.onload=()=>{
    document.getElementById("back").href = base_url;
};

function resettable() {
    $('#operations tbody').empty();
    $.ajax({
            url: base_url + "configurations",
            type: 'delete',
            success: function(data) {
                console.log(JSON.stringify(data))
            }
        }

    )
}

function refreshtable() {
    $('#operations tbody').empty();
    getconfigurations()
}
$(document).ready(
    getconfigurations()
);

$(function() {
    $("#uploadform").on("submit", function(event) {

        event.preventDefault();
        var files = $('#file')[0].files;

        var fd = new FormData();
        fd.append('file', files[0]);


        fd.append('option', $("input[name=option]:checked").val());

        $.ajax({
            url: base_url + "configure-archiver",
            type: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            data: fd,
            success: function(data) {
                if (data["error"] !== null && data["error"] !== '' && data["error"] !== undefined) {
                    if ($.isArray(data["error"]))
                    {
                        $(".result").append("Error Logs:"+"<br/>");
                        var data_array = data["error"];
                        for (let i = 0; i < data_array.length; i++)
                        {
                            $(".result").append(data["error"][i]+"<br/>");
                        }
                    }
                    else{
                        $(".result").html(data["error"]);
                    }
                } else {
                    $('#result').empty();
                    $('#operations tbody').empty();
                    if (typeof data["partial_error"] != "undefined")
                    {
                        var data_array = data["partial_error"];
                        for (let i = 0; i < data_array.length; i++)
                        {
                            $(".result").append(data["partial_error"][i]+"<br/>");
                        }
                    }
                    getconfigurations()
                }
            },
            error: function(data) {
                var error = JSON.stringify(data);
                alert(error)
            },
            complete: function(data) {
                $('#uploadform')[0].reset();
            }
        });
    });
})



$(function() {
    $("#downloadform").on("submit", function(event) {
        event.preventDefault();

        var eventsubscriber = document.getElementById("eventsubscriber").value;

        var url = base_url + "download-configuration/?eventsubscriber=" + eventsubscriber;

        fetch(url)
            .then(resp => {
                if ("application/octet-stream" === resp.headers.get("content-type")) {
                    resp.blob()
                        .then(async function(blob) {
                            $('#result').empty();
                            var url = window.URL.createObjectURL(blob);
                            const a = document.createElement('a');
                            a.style.display = 'none';
                            a.href = url;
                            document.body.appendChild(a);
                            let date = new Date().toJSON();
                            a.download = "attribute" + "_" + date + ".yaml"
                            a.click();
                            window.URL.revokeObjectURL(url);
                            $('#downloadform')[0].reset();
                        })
                } else {
                    resp.json()
                        .then(json => {
                            var error = json
                            $(".result").html(error["error"]);
                            $('#downloadform')[0].reset();
                        })
                }
            })

            .catch((error) => {
                alert(error)
            });

    });
})


function getconfigurations() {
    fetch(base_url + "configurations")
        .then(
            async function(resp) {
                const json = await resp.json();
                Object.entries(json).forEach(
                    ([key, value]) => {

                        var table = document.getElementById("operations");
                        var rowCount = table.rows.length;
                        if ((rowCount) == 0) {
                            var header = table.createTHead();
                            var row = header.insertRow(0);
                            var cell = row.insertCell(0);
                            cell.innerHTML = "TIMESTAMP"
                            var cell = row.insertCell(1);
                            cell.innerHTML = "OPERATION"
                            var cell = row.insertCell(2);
                            cell.innerHTML = "ATTRIBUTE NAME"
                            $('<button onclick="resettable()">Reset Table</button>').appendTo('#resettable');
                            $('<button onclick="refreshtable()">Refresh Table</button>').appendTo('#refreshtable');

                        }
                        var addlist = value["add"];
                        var changelist = value["change"];
                        var removelist = value["remove"];
                        var tbody = $('#operations tbody')[0];
                        if (typeof addlist != "undefined") {
                            for (let i = 0; i < addlist.length; i++) {
                                var add = addlist[i]
                                var rowCount = tbody.rows.length;
                                var row = tbody.insertRow(rowCount);
                                var cell = row.insertCell(0);
                                cell.innerHTML = key
                                var cell = row.insertCell(1);
                                cell.innerHTML = "ADD"
                                var cell = row.insertCell(2);
                                cell.innerHTML = add
                            }
                        }
                        if (typeof changelist != "undefined") {
                            for (let i = 0; i < changelist.length; i++) {
                                var change = changelist[i]
                                var rowCount = tbody.rows.length;
                                var row = tbody.insertRow(rowCount);
                                var cell = row.insertCell(0);
                                cell.innerHTML = key
                                var cell = row.insertCell(1);
                                cell.innerHTML = "CHANGE"
                                var cell = row.insertCell(2);
                                cell.innerHTML = change
                            }
                        }
                        if (typeof removelist != "undefined") {
                            for (let i = 0; i < removelist.length; i++) {
                                var remove = removelist[i]
                                var rowCount = tbody.rows.length;
                                var row = tbody.insertRow(rowCount);
                                var cell = row.insertCell(0);
                                cell.innerHTML = key
                                var cell = row.insertCell(1);
                                cell.innerHTML = "REMOVE"
                                var cell = row.insertCell(2);
                                cell.innerHTML = remove
                            }
                        }
                    }
                )
            }
        )
}


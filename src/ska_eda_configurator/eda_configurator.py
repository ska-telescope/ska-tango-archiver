"""This module contains API to add,update,remove and download archiver configurations."""
import datetime
import logging
import os
import subprocess
import time
from typing import List

import yaml
from fastapi import FastAPI, Form, Request, UploadFile
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import FileResponse, HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from ska_ser_logging import configure_logging

templates = Jinja2Templates(directory="templates")


app = FastAPI()


app.mount("/static", StaticFiles(directory="static"), name="static")
origins = [
    "http://k8s.stfc.skao.int",
    "https://k8s.stfc.skao.int",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

configuration_data = {}


class YamlError(KeyError):
    """Raised when yaml is invalid"""


class FileError(Exception):
    """Raised when File is invalid"""


class Yaml2ArchivingError(Exception):
    """Raised when yaml2archiving is provided with invalid file
    or invalid archiving data
    """


configure_logging()
logger = logging.getLogger("ska_eda_configurator")


# pylint: disable= too-many-locals
@app.post("/configure-archiver")
def create_upload_files(file: UploadFile, option: str = Form()):
    """Method for API to fetch files from the user and process it for adding or
    removing attributes from archiving.

    Args:
        file (UploadFile): File uploaded from the user
        option (str, optional): Option selected by the user either
        "add_update" or "remove". Defaults to Form().

    Returns:
        json: returns json response with processed output containing attributes
        added, changed and removed.
    """
    try:
        partial_error: str = ""
        filename = file.filename.split(".")[0] + "_" + str(time.time()) + ".yaml"
        folder_path = os.path.join(os.getcwd(), "uploaded_files")
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)
        file_path = os.path.join(folder_path, filename)
        with open(file_path, "wb") as serverfile:
            contents = file.file.read()
            if contents:
                serverfile.write(contents)
            else:
                raise FileError("Empty file")

        if option == "add_update":
            timestamp = datetime.datetime.now()
            result = subprocess.run(
                ["yaml2archiving", str(file_path), "-u", "-w", "-v"],
                capture_output=True,
                text=True,
                check=False,
            )

        elif option == "remove":
            timestamp = datetime.datetime.now()
            result = remove_attribute(file_path)

        if result.stdout:
            logger.info(result.stdout)
            if result.stderr:
                logger.error(result.stderr)
                partial_error = parse_error(result.stderr)
            current_add, current_change, current_remove = output_parser(timestamp, result.stdout)
            if partial_error:
                return {
                    "timestamp": timestamp,
                    "add": current_add,
                    "change": current_change,
                    "remove": current_remove,
                    "partial_error": partial_error,
                }
            return {
                "timestamp": timestamp,
                "add": current_add,
                "change": current_change,
                "remove": current_remove,
            }

        error = parse_error(result.stderr)

    except YamlError as yaml_error:
        error = str(yaml_error)
        logger.error(error)

    except AttributeError as atterror:
        error = str(atterror)
        logger.error("Invalid input file -> %s", error)

    except Exception as generalexception:
        error = str(generalexception)
        logger.error(error)

    return {
        "error": error,
    }


@app.get("/", response_class=HTMLResponse)
def add_config(request: Request):
    """Method for API renders index page with deployment type received from environment variable"""
    telescope = os.getenv("TELESCOPE_ENVIRONMENT")

    if telescope:
        title = f"EDA Configurator for {telescope} deployment"
    else:
        title = "EDA Configurator (unspecified deployment)"
    return templates.TemplateResponse(
        "index.html",
        {"request": request, "title": title},
    )


@app.get("/configurations")
def get_configurations():
    """Method for API used to get operations performed for archiver configuration."""
    return configuration_data


@app.delete("/configurations")
def delete_configurations():
    """Method for API used to delete operations performed for archiver configuration."""
    configuration_data.clear()
    return configuration_data


@app.get("/configuration-page", response_class=HTMLResponse)
def configure(request: Request):
    """Method for API used to render configuration page."""
    return templates.TemplateResponse("add_upload.html", {"request": request})


@app.get("/download-configuration/")
def download(eventsubscriber: str):
    """API to provide yaml file with archiver configuration present in the system."""
    try:
        file_name = "attribute_" + str(time.time()) + ".yaml"
        folder_path = os.path.join(os.getcwd(), "downloaded_files")
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)

        file_path = os.path.join(folder_path, file_name)

        out = subprocess.run(
            [
                "python3",
                "tools/archiving2yaml.py",
                eventsubscriber,
            ],
            capture_output=True,
            text=True,
            check=False,
        )
        if out.stderr:
            logger.error(out.stderr)
            raise Yaml2ArchivingError("Error logs:\n" + str(out.stderr))

        logger.info(out.stdout)

        with open(file_path, "w", encoding="UTF-8") as file:
            file.write(out.stdout)

        with open(file_path, "r+", encoding="UTF-8") as arch_stream:
            data = yaml.load(arch_stream, Loader=yaml.Loader)
            if not data.get("configuration"):
                raise FileError(
                    f"In EventSubscriber:{eventsubscriber}, no attributes are archived."
                )

        return FileResponse(
            path=file_path,
            media_type="application/octet-stream",
            filename=file_name,
        )
    except Exception as error:
        return {"error": str(error)}


# pylint: disable= too-many-locals
def remove_attribute(file_path: str):
    """
    Method used to take file and remove attributes from archiving
    present in the file.
    Args:
        filename (str): Name of yaml file with attributes.

    Raises:
        Exception: Exception raised if issue in file or attributes inside file.
        FileError: raised when file is empty
        YamlError: Error raised if yaml file is not valid.


    Returns:
        result(CompletedProcess[str]) : output from yaml2archiving tool.
    """
    # pylint: disable= consider-iterating-dictionary
    attr_class = {}
    with open(file_path, "r", encoding="UTF-8") as stream:
        data = yaml.load(stream, Loader=yaml.Loader)
        yamlvalidator(data)
        conf_manager = data.get("manager")
        event_subscriber = data.get("archiver")
        for conf_data in data.get("configuration"):
            attribute_list = []
            attributes_configurations = conf_data.get("attributes")
            if attributes_configurations:
                attribute_list = list(attributes_configurations.keys())
                if conf_data["class"] in list(attr_class.keys()):
                    attr_class[conf_data["class"]].extend(attribute_list)
                else:
                    attr_class[conf_data["class"]] = attribute_list
            else:
                raise YamlError("Missing attribute key")

    out = subprocess.run(
        [
            "python3",
            "tools/archiving2yaml.py",
            event_subscriber,
        ],
        check=True,
        capture_output=True,
        text=True,
    )
    logger.info(out.stdout)
    if out.stderr:
        # pylint: disable=broad-exception-raised
        raise Yaml2ArchivingError("Error logs:\n" + str(out.stderr))
    with open("attribute.yaml", "w", encoding="utf-8") as file:
        file.write(out.stdout)
    with open("attribute.yaml", "r+", encoding="utf-8") as arch_stream:
        data = yaml.load(arch_stream, Loader=yaml.Loader)
        data["manager"] = conf_manager
        classes = list(attr_class.keys())
        for conf_data in data.get("configuration"):
            if conf_data["class"] in classes:
                archived_attr = []
                attributes_configurations = conf_data.get("attributes")
                if attributes_configurations:
                    archived_attr = list(attributes_configurations.keys())
                    for attr in set(attr_class[conf_data["class"]]):
                        if attr.lower() in archived_attr:
                            conf_data.get("attributes").pop(attr.lower())
                    if not conf_data["attributes"]:
                        conf_data.pop("attributes")

    with open("attribute.yaml", "w", encoding="utf-8") as conf_stream:
        conf_stream.write(yaml.dump(data, sort_keys=False))
    result = subprocess.run(
        ["yaml2archiving", "attribute.yaml", "-w"], capture_output=True, text=True, check=False
    )

    return result


def parse_error(error_log: str) -> List[str]:
    """Method

    Args:
        error_log (str): Logs with all the details.

    Returns:
        list[str]: Parsed logs and maintained into a list.
    """
    errors: list = []
    for line in error_log.split("\n"):
        if (
            line.startswith("WARNING:")
            or line.startswith("ERROR:")
            or line.startswith("CRITICAL:")
        ):
            errors.append(line)
    return errors


def output_parser(timestamp: datetime, output: str):
    """Method to parse output data and extract data into lists.

    Args:
        output (str): Output from yaml2archiving tool

    Returns:
        add, change, remove (Tuple): lists with information of
        attributes added, changed and removed.
    """
    current_add, current_change, current_remove = [], [], []
    configuration_data.update({timestamp: {}})
    for line in output.split("\n"):
        if "SKIP" not in line:
            if "ADD" in line:
                current_add.append(line.split("ADD")[1])
            elif "CHANGE" in line:
                current_change.append(line.split("CHANGE")[1])
            elif "REMOVE" in line:
                current_remove.append(line.split("REMOVE")[1])
    configuration_data[timestamp].update(
        {
            "add": current_add,
            "change": current_change,
            "remove": current_remove,
        }
    )
    return current_add, current_change, current_remove


def yamlvalidator(data: dict):
    """
    Used to validate yaml before processing it.

    Args:
        data (dict): Contains  data from yaml file

    Raises:
     FileError: raised when file is empty
     YamlError: raises error if expected fields are not present in yaml file
    """
    if not data:
        raise FileError("Empty file")
    if not data.get("manager"):
        raise YamlError("manager key is missing in yaml file")
    if not data.get("archiver"):
        raise YamlError("archiver key is missing in yaml file")
    if not data.get("configuration"):
        raise YamlError("configuration key is missing in yaml file")
    for configuration in data.get("configuration", []):
        if not isinstance(configuration, dict):
            raise YamlError("configurations not valid")
        if not configuration.get("attributes"):
            raise YamlError("attributes key is missing inside configuration field")
        if not configuration.get("class"):
            raise YamlError("class key is missing inside configuration field")
